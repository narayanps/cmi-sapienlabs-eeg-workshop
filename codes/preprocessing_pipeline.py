#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 11:34:27 2023

@author: NARAYAN SUBRAMANIYAM
THIS CODE PERFORMS PREPROCESSING OF EEG DATA . THE STEPS IMPLEMENTED ARE FILTERING
AND ICA ARTIFACT REHECTION.
"""
import os
import numpy as np
import mne
from mne.preprocessing import ICA, create_eog_epochs


#LOAD DATA
fname = #point to .edf file downloaded from git
raw = mne.io.read_raw_edf(fname, preload=(True)) #instance of the mne.io.Raw class
print(raw.info) # print info about the data e.g. channel names , sampling freq etc
ch_names = raw.info['ch_names']
print(ch_names)


#PICK ONLY EEG CHANNELS
eeg_chs_list = list(range(2,16))
all_chans = range(0,len(ch_names))
unwanted_chans = [x for x in all_chans if x not in eeg_chs_list]
raw.drop_channels(ch_names=[ch_names[i] for i in unwanted_chans])
samp_freq=raw.info['sfreq']
eeg_ch_names=raw.info['ch_names']

#GET THE MONTAGE
montage = mne.channels.read_custom_montage('/worktmp/nbnapu/course_eeg/chan_14.locs', head_size=0.095, coord_frame=None)
raw.set_montage(montage)

#PLOT RAW DATA
#PLOT
raw.plot(duration=60,remove_dc=True) # what happens if remove_dc=False ?

#PLOT PSD AVERAGED OVER CHANNELS
fig = raw.compute_psd(tmax=np.inf).plot(average=True)

#BANDPASS FILTER THE DATA
raw.filter(l_freq=1, h_freq=40)

#PLOT PSD AVERAGED OVER CHANNELS AFTER FILTERING
fig = raw.compute_psd(tmax=np.inf).plot(average=True)

#################### EXERCISE  : Plot the amplitudes before and after filtering 
#on the same plot. Note that we are manipulating the raw object without making a copy.
#So either plot first or work with a copy, else you will be plotting the same data!#####################################

#RUN ICA TO DETECT ARTIFACTS
filt_raw = raw.copy().filter(l_freq=1.0, h_freq=None) #HPF at 0.1 Hz to better see the EOG component
ica = ICA(n_components=14, max_iter="auto", random_state=97)
ica.fit(filt_raw)
ica
ica.plot_sources(raw,show_scrollbars=True)

# EXPLAINED VARIANCE
explained_var_ratio = ica.get_explained_variance_ratio(filt_raw)
for channel_type, ratio in explained_var_ratio.items():
    print(
        f"Fraction of {channel_type} variance explained by all components: " f"{ratio}"
    )

explained_var_ratio = ica.get_explained_variance_ratio(
    filt_raw, components=[0], ch_type="eeg"
)
# This time, print as percentage.
ratio_percent = round(100 * explained_var_ratio["eeg"])
print(
    f"Fraction of variance in EEG signal explained by first component: "
    f"{ratio_percent}%"
)


#PLOT ICA SOURCES
ica.plot_sources(raw, show_scrollbars=False)

#VISUALIZE ICA COMPONENTS USING TOPOGRAPHY
ica.plot_components()

#RUN MORE DIAGNOSTICS
ica.plot_properties(raw, picks=[0, 1]) #CHECK COMPONENT 0 AND 1 for example, SIMILARLY CHECK MORE COMPONENTS

#IF YOU KNOW WHICH COMPONENT TO REMOVE, FIRST CHECK HOW THIS WOULD COMPARE TO THE ORIGINAL DATA BY 
#USING OVERLAY METHOD. THIS WILL INTERNALLY MAE A COPY AND SHOW WHAT EXCLUDING A ic WOULD DO

#OVERLAY
ica.plot_overlay(raw, exclude=[4], picks="eeg")

#IF YOU ARE CONVINCED THAT EOG ARTIFACTS ARE SUPPRESSED AND DATA IS NOT TOO
#DISTORTED, REMOVE THE COMPONENT

#SELECT COMPONENTS MANUALLY
ica.exclude = [] # ENTER THE COMPONENT THAT LOOK LIKE EOG TO EXCLUDE
reconst_raw = raw.copy()
ica.apply(reconst_raw)
raw.plot(duration=60, show_scrollbars=True)
reconst_raw.plot(duration=60, show_scrollbars=True)


#DETECTING COMPONENTS AUTOMATICALLY - WE DONT HAVE AN EOG CHANNEL SO CANT USE THIS!
ica.exclude = []
# find which ICs match the EOG pattern
###eog_indices, eog_scores = ica.find_bads_eog(raw,ch_name=['AF3'],measure='zscore')
###ica.exclude = eog_indices





