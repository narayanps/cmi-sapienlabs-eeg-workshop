#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 11:17:46 2023

@author: nbnapu
"""
import mne

def filter_eeg(raw, l_freq=None, h_freq=40, method='fir', phase = 'zero', picks='eeg'):
    raw.filter(l_freq=l_freq, h_freq=h_freq, picks=picks, method=method, phase=phase)
    return raw


def make_epochs(raw, events, event_id_dict,tmin, tmax,reject=None):
    epochs = mne.Epochs(raw,events=events, event_id=event_id_dict, tmin=tmin, 
                        tmax=tmax, reject=reject, preload=True)
    return epochs

def make_evoked(epochs,picks='eeg'):
    return epochs.average(picks=picks)
    



# Define the path where your data is stored
sample_data_folder = mne.datasets.sample.data_path()
sample_data_raw_file = (
    sample_data_folder / "MEG" / "sample" / "sample_audvis_filt-0-40_raw.fif"
)


#Use MNE's IO function to read the data
raw = mne.io.read_raw_fif(sample_data_raw_file, preload=(True))

#plot raw data
raw.plot(duration=5, n_channels=10)


#Detect events
events = mne.find_events(raw, stim_channel="STI 014")
print(events[:5])  # show the first 5

#Let's use a dictionary to map event IDs to conditions
event_id_dict = {
    "auditory/left": 1,
    "auditory/right": 2,
    "visual/left": 3,
    "visual/right": 4,
    "smiley": 5,
    "buttonpress": 32,
}

#filter the data
filt_data_1  = filter_eeg(raw.copy(),l_freq=1, h_freq=40, method='fir', phase = 'zero', picks='eeg')
filt_data_2  = filter_eeg(raw.copy(),l_freq=1, h_freq=40, method='fir', phase = 'zero-double', picks='eeg')
filt_data_3  = filter_eeg(raw.copy(),l_freq=0.1, h_freq=40, method='fir', phase = 'minimum', picks='eeg')

#reject data based on thresholds
reject_criteria = dict(
    mag=4000e-15,  # 4000 fT
    grad=4000e-13,  # 4000 fT/cm
    eeg=150e-6,  # 150 µV
    eog=250e-6,
)  # 250 µV

#epoch the data
tmin=-0.2
tmax=0.5
epochs_orig = make_epochs(raw, events, event_id_dict, tmin, tmax,reject=None)
epochs_1 = make_epochs(filt_data_1, events, event_id_dict, tmin, tmax,reject=None)
epochs_2 = make_epochs(filt_data_2, events, event_id_dict, tmin, tmax,reject=None)
epochs_3 = make_epochs(filt_data_3, events, event_id_dict, tmin, tmax,reject=None)
    

#combine epochs and look at visual stimulus data

conds = ["auditory/left", "auditory/right", "visual/left", "visual/right"]
epochs_orig.equalize_event_counts(conds)  # this operates in-place

vis_epochs_orig = epochs_orig["auditory"]
del raw, epochs_orig  # free up memory

epochs_1.equalize_event_counts(conds)  # this operates in-place
vis_epochs_1 = epochs_1["auditory"]
del filt_data_1, epochs_1  # free up memory

epochs_2.equalize_event_counts(conds)  # this operates in-place
vis_epochs_2 = epochs_2["auditory"]
del filt_data_2, epochs_2  # free up memory

epochs_3.equalize_event_counts(conds)  # this operates in-place
vis_epochs_3 = epochs_3["auditory"]
del filt_data_3, epochs_3  # free up memory


#create evoked data and plot
vis_evoked_orig = make_evoked(vis_epochs_orig,picks='eeg')
#vis_evoked_orig.plot()

vis_evoked_1 = make_evoked(vis_epochs_1,picks='eeg')
#vis_evoked_1.plot()

vis_evoked_2 = make_evoked(vis_epochs_2, picks='eeg')
#vis_evoked_2.plot()

vis_evoked_3 = make_evoked(vis_epochs_3,picks='eeg')
#vis_evoked_3.plot()


mne.viz.plot_compare_evokeds(
    dict(orig=vis_evoked_orig,filt_1=vis_evoked_1,filt_2=vis_evoked_2),
    legend="upper left",
    show_sensors="upper right",picks='EEG 054'
)