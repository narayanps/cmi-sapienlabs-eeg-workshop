#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 22:34:36 2023

@author: Narayan Subramaniyam
This code illustrates how convolution is same as computing moving averages and 
eventually demonstrates how this is a low-pass filtering or smoothing operator.
The code also plots the impulse response in frequency domain
"""
import numpy as np
import matplotlib.pyplot as plt
import math

# Generate synthetic EEG data
np.random.seed(0)
num_samples = 1000
eeg_data = np.random.normal(0, 1, num_samples)


# Implement moving average using averaging
window_size = 5
averaging_weights = np.ones(window_size) /window_size
moving_avg_averaging = np.convolve(eeg_data, averaging_weights, mode='same')

# Implement moving average using convolution
moving_avg_convolution = np.convolve(eeg_data, np.ones(window_size), mode='same') / window_size

# Plot original data, moving averages and numpy's convolve output
plt.figure(figsize=(10, 6))
plt.plot(eeg_data, label='Original EEG Data')
plt.plot(moving_avg_averaging, label='Moving Average (Averaging)')
plt.plot(moving_avg_convolution, label='Moving Average (Convolution)')
plt.xlabel('Sample')
plt.ylabel('Amplitude')
plt.title('Synthetic Data with Moving Average filtering')
plt.legend()
plt.grid(True)
plt.show()


#Frequency response via FFT
plt.figure()
nfft=64
weights_ = np.zeros((nfft,1))
weights_[0:window_size,0] = averaging_weights
fc = 2*np.abs(np.fft.fft(weights_,nfft,axis=0))/weights_.shape[0] #fourier coefficients
fs=1000
freqs1 = np.linspace(0, fs/2, math.floor(weights_.shape[0]/2 + 1))
plt.plot(freqs1, 10*np.log10(fc[0:len(freqs1)]))
plt.xlabel('Frequencies')
plt.ylabel('Magnitude (dB)')
#Try plotting the phase reponse! via FFT


