#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 11:59:57 2023

@author: NARAYAN SUBRAMANIYAM
Functions to compute psd using FFT, Welch algotithm. Also inlcudes Morlet method to do TF analysis.
# INPUT is a numpy array. Rember to specify the right axis along which to compute the spectral measures.
"""
import numpy as np
from scipy import signal
from mne.time_frequency import tfr_array_morlet


def fft_psd(data, N, axes=None):
    psd = (2*np.abs(np.fft.fftn(data,axes=(axes,))/N) )**2   
    return psd

def welch_psd(data, fs, axis=-1, window='hann', nperseg=None, noverlap=None, nfft=None):
   frequencies, psd = signal.welch(data, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap, nfft=nfft,axis=axis)
   return frequencies, psd

def morlet_tf(data,fs, freqs,n_cycles, output='avg_power'):
    if len(data.shape) !=3:
        raise ValueError('Data should be 3 dimensional - trials X chans X times')
    return tfr_array_morlet(
        data,
        sfreq=fs,
        freqs=freqs,
        n_cycles=n_cycles,
        output=output,
    )