#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 15:57:25 2023

@author: NARAYAN SUBRAMANIYAM
THIS CODE COMPUTES VARIOUS ENTROPY METRICS FOR SINGLE CHANNEL EEG DATA
"""
import numpy as np
from itertools import permutations
import math
import scipy
import mne
from mne.time_frequency import psd_array_welch

def embed_ts(ts,m=3,tau=1):
  #EMBEDDING THE DATA TO MAKE WINDOWS. USE DEFAULT VALUES OF m=3 and tau=1
    N=len(ts)
    M=N-(m-1)*tau
    emb_vec=np.empty((M,m), dtype="float32")
    
    for i in range(0, m):
        inds=np.arange(0,M)+i*tau
        emb_vec[:,i] = ts[inds].transpose()
    return emb_vec


def sample_entropy(ts, m=3, tau=1, r=0.1, p='inf'):
  #COMPUTES SAMPLE ENTROPY. REF : https://sapienlabs.org/lab-talk/measuring-entropy-in-the-eeg/
    emb = embed_ts(ts,m=m, tau=tau)
    emb1 = embed_ts(ts,m=m+1, tau=tau)
    w=scipy.spatial.KDTree(emb)
    w1=scipy.spatial.KDTree(emb1)
    if p=='inf':
        u=scipy.spatial.KDTree.query_ball_point(w,emb,r*np.std(ts),np.inf)
        u1=scipy.spatial.KDTree.query_ball_point(w1,emb1,r*np.std(ts),np.inf)
    else:
        u=scipy.spatial.KDTree.query_ball_point(w,emb,r*np.std(ts),p)
        u1=scipy.spatial.KDTree.query_ball_point(w1,emb1,r*np.std(ts),p)
    A = np.asarray([len(arr)-1 for arr in u]) / (emb.shape[0] - 1)
    B = np.asarray([len(arr)-1 for arr in u1]) / (emb1.shape[0] - 1)
    return -np.log2(np.mean(B)/np.mean(A))

def permutation_entropy(ts, m=3, tau=1):
    """
    """
    #PERMUTATION ENTROPY REF : https://onlinelibrary.wiley.com/doi/full/10.1111/anzs.12376#:~:text=Permutation%20entropy%20(PE)%20is%20an,or%20non%2Dlinear%20time%20series.
    emb = embed_ts(ts,m=m, tau=tau)
    transition_network=np.empty((emb.shape[0], 1),dtype='float32')
    all_poss_patterns=list(permutations(list(np.arange(0,m))))
    for i in range(emb.shape[0]):
         order = np.argsort(emb[i,:])
         rank = list(np.argsort(order))
         transition_network[i,:]=all_poss_patterns.index(tuple(rank))
    
    symbols_, count_ = np.unique(transition_network, return_counts=True, axis=0)
    prob = count_/count_.sum()
    pe   = -np.sum(prob*np.log2(prob))
    pe_max = np.log2(float(math.factorial(m)))
    return pe/pe_max

def spectral_entropy(ts, sfreq=1000,fmin=1, fmax=1000, n_per_seg=256, n_fft=256):
  #SPECTRAL ENTROPY - ENTROPY IN FREQUENCY DOMAIN
    psd, freqs = psd_array_welch(ts, sfreq, fmin=fmin, fmax=fmax, n_per_seg=n_per_seg,n_fft=n_fft)
    psd_norm = psd / psd.sum()
    sp_e = -np.sum(psd_norm*np.log2(psd_norm))
    return sp_e/np.log2(psd_norm.shape[0])