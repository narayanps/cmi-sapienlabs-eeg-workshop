#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 16:41:17 2023

@author:NARAYAN SUBRAMANIYAM
THIS CODE CALCULATES VARIOUS SPECTRAL CONNECTIVITY MEASURES.
THE DATA SHOULD BE AN ARRAY TRIALSXCHANSXTIME
"""
import numpy as np
from scipy import signal
import compute_spectral_measures


def compute_csd_matrix(data,samp_freq, nfft):
  #COMPUTE CROSS SPECTRAL DENSITY  AND AVERAGE OVER TRIALS. REFER TO LECTURE SLIDES
    if len(data.shape) != 3:
        raise ValueError('Data should be 3 dimensional - trials X chans X times')
    num_trials = data.shape[0]
    num_signals=data.shape[1]
    # Array to hold cross-spectral density matrix averaged over trials
    csd_all = np.zeros((num_signals, num_signals, nfft//2 + 1), dtype=np.complex128)
    for k in range(num_trials):
        csd_matrix = np.zeros((num_signals, num_signals, nfft//2 + 1), dtype=np.complex128)
        for i in range(num_signals):
            for j in range(num_signals):
                freqs, csd = signal.csd(data[k,i,:].squeeze(), data[k,j,:], fs=600, nfft=nfft)
                csd_matrix[i, j, :] = csd
        csd_all += csd_matrix # add CSD for each trial
    csd_mean = csd_all/num_trials # average over trials
    return freqs, csd_mean

def compute_coherence_coefficient(csd, freqs, frange=None):
  #COHERENCE COEFFICIENT : REFER TO LECTURE SLIDES
    f_indx = np.zeros((2,1),dtype=int)
    if frange == None:
        f_indx[0]=0
        f_indx[1] = len(freqs)
    else:
        f_indx[0] = np.argmin(np.abs(freqs-frange[0]))
        f_indx[1] = np.argmin(np.abs(freqs-frange[1]))
    num_freqs = len(range(f_indx[0].item(), f_indx[1].item()+1))
    coh = np.zeros((csd.shape[0],csd.shape[0],num_freqs))
    for f in range(num_freqs):
        for i in range(csd.shape[0]):
            for j in range(i+1, csd.shape[0]):
                idx = f_indx[0].item()+f
                S_xy = np.abs(csd[i,j,idx])
                S_xx = np.abs(csd[i,i,idx])
                S_yy = np.abs(csd[j,j,idx])
                coh[i,j,f] = (S_xy/np.sqrt(S_xx*S_yy))
    return freqs[f_indx[0].item():f_indx[1].item()+1],coh


def compute_plv_pli(data,fs,n_cycles,f):
  #COMPUTE PLI AND PLV USING MORLET WAVELETS
    if len(data.shape) != 3:
       raise ValueError('Data should be 3 dimensional - trials X chans X times')
      
    complex_ = compute_spectral_measures.morlet_tf(data, fs=fs,freqs=[f], 
                                              n_cycles=n_cycles,output="complex")
    complex_ = np.mean(complex_.squeeze(),axis=0)
    #extract angles
    angle_1=np.angle(complex_[0,:])
    angle_2=np.angle(complex_[1,:])
    theta_values = angle_1 - angle_2
    plv = np.abs(np.mean(np.exp(1j*theta_values)))
    pli = np.abs(np.mean(np.sign(np.angle(np.exp(1j*theta_values)))))
    return plv, pli
