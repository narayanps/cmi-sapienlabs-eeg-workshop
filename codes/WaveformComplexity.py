import mne
import numpy as np
from random import choices
from scipy import stats
from itertools import combinations

#Function to extract the header of EDF file
def GetEDFMetaData_EDF(file):
    data = mne.io.read_raw_edf(file)
    raw_data = data.get_data()
    # you can get the metadata included in the file and a list of all channels:
    info = data.info
    channels = data.ch_names

    ch = channels.copy()
    rostral = ['Fp', 'AF', 'F', 'FT', 'FC', 'T', 'TP', 'CP', 'P', 'PO', 'O', 'A', 'Pg', 'I']
    lateral = ['z'] + list(range(1, 11))
    ChannelNames = [(x + str(y)) for x in rostral for y in lateral]

    ## EEG_channels = [value for value in ch if value in ChannelNames]
    EEG_channels = set(ch).intersection(ChannelNames)
    indices_channel = [ch.index(x) for x in EEG_channels]

    Other_channels = [value for value in ch if value not in ChannelNames]

    rec_duration = raw_data.shape[1] / data.info["sfreq"]

    metadata = dict(filepath=file,
                    startdate=data.info["meas_date"],
                    recording_duration=rec_duration,
                    samplingrate=data.info["sfreq"],
                    EEGChannels=EEG_channels,
                    indices_channel=indices_channel,
                    OtherChannels=Other_channels)
    return (metadata)

#Function to calculate waveform complexity EDF file.
#Preprocessing : only mean correction.
def WaveformComplexityFromEDF(file, segment_duration=750, signal_percent=0.4):
    data = mne.io.read_raw_edf(file)
    raw_data = data.get_data()

    meta_data = GetEDFMetaData_EDF(file)  # Meta data incl channel names, sampling frequency from EDF meta data
    ChannelNames = meta_data['EEGChannels']
    Fs = meta_data['samplingrate']
    max_signal = meta_data['recording_duration']
    ChannelNames = list(meta_data['EEGChannels'])
    ChannelIndex = list(meta_data['indices_channel'])
    Fs = meta_data['samplingrate']
    max_signal = meta_data['recording_duration']

    ##number of segments used to estimate complexity
    nSegments = int(max_signal * 1000 * signal_percent // segment_duration)
    ChannelComplexity = {}
    GapArrayList = []

    count = 0
    for k in ChannelIndex:
        channel = ChannelNames[count]

        # mean correction. Trace contains eeg voltage values
        raw_signal = raw_data[k, :]
        trace = raw_signal - np.mean(raw_signal)
        del raw_signal

        if count == 0:
            nFrames_per_segment = segment_duration * Fs // 1000
            nGaps = nSegments
            selected_trace_length = nSegments * nFrames_per_segment
            maxGap = int((len(trace) - selected_trace_length) / nGaps)  ##length of trace

            if maxGap < 2:
                nSegments = len(trace) // (nFrames_per_segment + 2)
                print('max segments reset to', nSegments)
                selected_trace_length = nSegments * nFrames_per_segment
                maxGap = int((len(trace) - selected_trace_length) / nSegments)  ##length of trace

            ##the segments start points are different for every channel
            GapArrayList = [choices(range(maxGap), k=nSegments) for x in ChannelNames]

        ## an array of randomly selected gaps between the segments
        GapArray = GapArrayList[count]

        ## Getting Startpoint/Endpoint of segments
        startPts = np.cumsum(GapArray) + [x * nFrames_per_segment for x in range(nSegments)]
        startPts = startPts.tolist()
        startPts = [int(item) for item in startPts]
        endPts = [int(x + nFrames_per_segment - 1) for x in startPts]

        ##create a data frame of all segments and get the correlation values for all combinations
        allSegments = [trace[startPts[x]:endPts[x]] for x in range(nSegments)]
        corrVal = [stats.pearsonr(allSegments[x], allSegments[y])[0] for x, y in
                   combinations(range(len(allSegments)), 2)]
        distVal = [(1 - abs(number)) for number in corrVal] ##Distance = 1-Correlation

        complexity = int(round(np.median(distVal) * 100, 0))
        ##calculating complexity from correlation values
        ChannelComplexity[channel] = complexity

        count += 1

    return (ChannelComplexity)

file = '../data/16160_EyesClosedPassive.edf' #change file path to appropriate location
cw = WaveformComplexityFromEDF(file)

################################Exercise to do#################################################################
##Exercise 1: Plot histogram to compare complexity b/w 2 tasks across 28 individuals - "EyesClosedPassive" vs "EyesOpenVisualTask" task (https://drive.google.com/drive/folders/166aUH8_7MfVCrT9tZCfumiInOhxIUGqr?usp=drive_link)


##Optional Exercise 2: What is the impact of filtering on waveform complexity. Use pattern completion dataset - "EyesClosedPassive" task (https://drive.google.com/drive/folders/166aUH8_7MfVCrT9tZCfumiInOhxIUGqr?usp=drive_link)
## Calculate mean complexity for each EDF file. Plot histogram of complexity  with and without filter.
