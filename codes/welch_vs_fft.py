#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 22:58:29 2023

@author: nbnapu
"""
import numpy as np
import matplotlib.pyplot as plt
import math
import mne
from func import compute_spectral_measures

#load data
fname = # point to the .edf file that you downloaded from gitlab
raw = mne.io.read_raw_edf(fname, preload=(True)) #instance of the mne.io.Raw class
print(raw.info) # print info about the data e.g. channel names , sampling freq etc
ch_names = raw.info['ch_names']
print(ch_names)


#PICK ONLY EEG CHANNELS
eeg_chs_list = list(range(2,16))
all_chans = range(0,len(ch_names))
unwanted_chans = [x for x in all_chans if x not in eeg_chs_list]
raw.drop_channels(ch_names=[ch_names[i] for i in unwanted_chans])
fs=raw.info['sfreq']
eeg_ch_names=raw.info['ch_names']
eeg_data = raw.get_data()
ch_no=10 #choose a channel to plot


N=eeg_data.shape[1] #number of data samples
nfft = math.floor(N/2 + 1) # number of nfft points
freqs = np.linspace(0, fs/2, nfft) # freq spacing based on nfft
fft_psd = compute_spectral_measures.fft_psd(eeg_data,N, axes=-1) #compute pwr spect based on FFT for all the channes 
plt.figure()
plt.plot(freqs, 20*np.log10(fft_psd[ch_no,0:len(freqs)]))
plt.xlabel('Frequency')
plt.ylabel('Power (dB)')
plt.title('FFT')




#welch method
plt.figure()
nperseg = 1*fs # window length = 1sec
nfft = 1*fs # nfft = no of data points in the window
noverlap = nperseg//4
freqs, welch_psd = compute_spectral_measures.welch_psd(eeg_data, fs, axis=-1, window='hann', nperseg=nperseg, noverlap=noverlap, nfft=nfft)
plt.plot(freqs, 20*np.log10(welch_psd[ch_no,0:len(freqs)]))
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.title('Welch method')


################################Exercise to do#################################################################

#  1. Average the PSDs for both FFT and Welch over all the channels and plot as
# function of frequency along with standard deviation. How variable is the PSD
#over all channels ? Are some PSDs at some frequencies more variable than the 
# other ?

## 2. Change the window size and overlapping parameter in the welch algorithm 
## and see how the psd changes. Do you gain/lose anything by increasing/decreasing window size ?
# what about the overlapping parameter ?

## 3. Bonus! How does choice of the tapering function affect the output ?

## Email your results as zip file including a small report answering the questions along with .py file. 
# Name your file with your first name_soln_psd_vs_welch.

################################################################################################################